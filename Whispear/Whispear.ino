#define MAX_CHAR_LEN 20
#define MAX_CHECK_PER_LOOP 5
#define MAX_CHECK_COUNT 5

//ForOutput
const char outputValveMessageOpen = 'O';
const char outputValveMessageClose = 'C';
const int  safetyValvePort = 9;
const int  outputValvePort = 10;

const char pumpMessage = 'P';
const int  pumpPort = 5;
const char lightMessage = 'L';
const int  lightPort = 3;

//ForInput
const char thermoMessage = 'T';
const int  baseThermoPort = 0;
const int  targetThermoPort = 1;
double baseThermoVal = 0.0;
double targetThermoVal = 0.0;
int thermoCheckCounter = 0;

//Other
char buffer[MAX_CHAR_LEN];
int  bufferCounter = 0;
char mode = 'S';

void ValveControl(boolean,int);
void OutputControl(boolean);
void ThermoCheck();


void setup() {
  //シリアル通信初期化
  Serial.begin(115200);
  pinMode(pumpPort,OUTPUT);
}

void loop() {
  if (Serial.available() > 0) {
    //シリアル通信で送られてきたメッセージをチェック
    //メッセージ形式は
    //M...
    char tmp=' ';
    
    switch (mode) {
      case 'S':  //初期状態
        tmp = Serial.read();
        mode = tmp;
        bufferCounter = 0;
        break;

      case outputValveMessageOpen:
        ValveControl(true,outputValvePort);
        break;

      case outputValveMessageClose:
        ValveControl(false,outputValvePort);
        break;

      case lightMessage:
        OutputControl(true);
      break;
      
      case pumpMessage:
        OutputControl(false);
        break;
        
      default:
        mode = 'S';
        break;
    }
  }else ThermoCheck();
  //delay(1);
}

void ValveControl(boolean toOpen, int port){
  if(toOpen){
    digitalWrite(port,HIGH);
  }else{
    digitalWrite(port,LOW);
  }
}

void OutputControl(bool light){
  //シリアル入力を一つ読み込み、/が来たら
  //それまでの入力をintに変換してpumpに出力
  int a = 0; char tmp;
  tmp = Serial.read();
  buffer[bufferCounter++] = tmp;
  if (tmp == '/') {
    //末尾に終端文字付加
    buffer[bufferCounter - 1] = '\0';
    
    //intに変換
    a = atoi(buffer);
    if(a>255) a = 255;
    
    if(a==0 ){
      if(light) digitalWrite(lightPort,LOW);
      else digitalWrite(pumpPort,LOW);
    }else if (a > 0 ){
      if(light) analogWrite(lightPort,a);
      //digitalWrite(lightPort,HIGH); 
      else analogWrite(pumpPort,a);
      //digitalWrite(pumpPort,HIGH);
    }
    
    //初期状態へ
    mode = 'S';
    bufferCounter = 0;
  }

  //入力が長すぎる場合は棄却
  if (bufferCounter >= MAX_CHAR_LEN ) {
    mode = 'S';
    bufferCounter = 0;
  }
  
}

void ThermoCheck() {
  //温度を数回に分けて取得し、
  //平均をシリアルで送信
  int i = 0;
  
  for (i = 0; i < MAX_CHECK_PER_LOOP; i++) {
    baseThermoVal = baseThermoVal + (double)analogRead(baseThermoPort);
    targetThermoVal = targetThermoVal + (double)analogRead(targetThermoPort);
  }
  
  thermoCheckCounter += MAX_CHECK_PER_LOOP;
  if (thermoCheckCounter >= MAX_CHECK_COUNT) {
    //MAX_CHECK_COUNT回データを取って平均
    float t1 = (double)baseThermoVal;
    float t2 = (double)targetThermoVal;
    
    int count = MAX_CHECK_COUNT;
    t1 = (t1/(double)count);
    t2 = (t2/(double)count);
    
    // 読み込んだ角度値をそのまま転送
    Serial.print(thermoMessage);
    Serial.print(t1);
    Serial.print("/");
    Serial.print(t2);
    Serial.println("E");    
    thermoCheckCounter = 0;
    baseThermoVal = 0.0;
    targetThermoVal = 0.0;
  }

}

