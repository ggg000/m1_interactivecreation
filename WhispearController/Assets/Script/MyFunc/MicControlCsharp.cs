﻿using UnityEngine;
using System.Collections;

public class MicControlCsharp : MonoBehaviour {
    TBE_3DCore.TBE_Source mSource;
    public float mDelayTime = 1f;

	// Use this for initialization
	void Start () {
        mSource = GetComponent<TBE_3DCore.TBE_Source>();
        mSource.clip = Microphone.Start(null, true, 999, 44100);

        if (mSource.clip == null)
        {
            Debug.LogError("Microphone.Start");
        }

        mSource.loop = true;
        mSource.mute = false;

        while (!(Microphone.GetPosition(null) > 0)) { Debug.Log("Waiting"); }
        mSource.PlayDelayed(mDelayTime);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
