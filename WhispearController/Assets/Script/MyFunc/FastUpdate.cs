﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 高速更新を制御するクラス
/// 登録しておいた各クラスの高速更新を制御
/// </summary>
public class FastUpdate : MonoBehaviour
{
    [SerializeField]
    private UpdateInterface[] mUpdateTargets;
    [SerializeField]
    private float mFPS = 1000f;
    [SerializeField]
    private float mMaximumFPS = 2000f;
    [SerializeField]
    private bool mFirstOn = true;
    private float mSPF;
    private float mPreFpsSetTime = 0f;
    private float mPreUpdateTime = 0f;
    private int mFrame = 0;
    private bool mStopUpdate = false;

#pragma warning disable 414
    private float mCurrentFPS = 0f;
#pragma warning restore 414


    // Use this for initialization
    void Start()
    {
        mSPF = 1f / mFPS; //FPSからフレーム間時間計算
        mPreUpdateTime = -1f * mSPF;
        InvokeRepeating("FastUpdateBase", 0.0001f, 1f / mMaximumFPS);

        //最初は更新しない?
        mStopUpdate = !mFirstOn;
    }

    //高速更新をするかどうか
    public void SetUpdateFlag(bool flag)
    {
        mStopUpdate = !flag;
    }

    private void FastUpdateBase()
    {

        float now = Time.realtimeSinceStartup;
        float delta = (now - mPreUpdateTime);


        //高速更新 
        //SPF分時間が立ったら一回更新
        //ただし、一回で複数回分の時間がたっていればその分更新
        while (delta > mSPF)
        {
            mFrame += 1;
            if (!mStopUpdate) TargetsUpdate(mSPF);
            mPreUpdateTime += mSPF;

            delta = now - mPreUpdateTime;
        }

        //FPS表示
        float time = (Time.realtimeSinceStartup - mPreFpsSetTime);
        if (time > 0.1f)
        {
            mCurrentFPS = mFrame / time;
            mFrame = 0;
            mPreFpsSetTime = Time.realtimeSinceStartup;
        }
    }

    //登録済みのクラスを高速更新
    private void TargetsUpdate(float spf)
    {
        foreach (UpdateInterface ui in mUpdateTargets)
        {
            ui.FastUpdate(spf);
        }
    }
}
