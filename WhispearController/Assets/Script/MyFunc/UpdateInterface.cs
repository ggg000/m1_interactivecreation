﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 高速更新を行うクラスの基底クラス
/// </summary>
public abstract class UpdateInterface : MonoBehaviour
{
    public abstract void FastUpdate(float spf);
}
