﻿using UnityEngine;
using System;
using System.Collections;
using System.IO.Ports;
using System.Threading;

/// <summary>
/// シリアル通信でバルブに指令を送る
/// </summary>
public class ValveController : MonoBehaviour
{
    public string mOutputValveMessageOpen = "O";
    public string mOutputValveMessageClose = "C";

    private bool mIsOpen = false;

    public int mPortNumber = 0;

    private static SerialPort mSerialPort;
    private int mRewriteCount = 0;
    public static bool mUsing = false;
    public static System.Object lockObj = new System.Object();

    void Start()
    {
        mSerialPort = ArduinoConnection.serialPort(mPortNumber);
        CloseValve();
    }

    public bool IsOpen()
    {
        return mIsOpen;
    }

    //出力バルブを開く
    //開いていたら無視
    public void OpenValve()
    {
        if (mIsOpen) return;
        mIsOpen = true;
        SendMessageValve(mOutputValveMessageOpen);
    }

    //出力バルブを閉じる
    //閉じていたら無視
    public void CloseValve()
    {
        if (!mIsOpen) return;
        mIsOpen = false;
        SendMessageValve(mOutputValveMessageClose);
    }

    //メッセージ送信
    private void SendMessageValve(string mess1)
    {
        lock (lockObj) //同時には書き込まない
        {
            if (mSerialPort != null && mSerialPort.IsOpen)
            {
            label:
                try
                {
                    mSerialPort.Write(mess1);
                    mRewriteCount = 0;
                }
                catch (Exception e)
                {
                    if (mRewriteCount < 10)
                    {
                        //失敗したら10回まで繰り返す
                        mRewriteCount += 1;
                        goto label;
                    }
                    else Debug.Log(e);
                }
            }
            //else StartCoroutine(ReWrite(mess1,mess2)); //失敗時はしばらく待ってもう一度
        }
    }

    IEnumerator ReWrite(string mess1)
    {
        yield return new WaitForSeconds(0.1f);
        SendMessageValve(mess1);
    }


    public GUISkin mSkin;
    public Rect mLabelRect;
    public bool mDebugShow = true;

    //実際に出力する電圧を表示
    void OnGUI()
    {
        if (mDebugShow)
        {
            if (mSkin) GUI.skin = mSkin;

            string open="";
            if (mIsOpen) open = "OPEN";
            else open = "CLOSE";

            GUI.Label(mLabelRect, "Valve(Output) :" + open);
        }
    }
}
