﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;

/// <summary>
/// Arduinoとのポート接続を行い、接続済みのシリアルポートを提供するクラス
/// </summary>
public class ArduinoConnection : MonoBehaviour
{
    private static SerialPort[] _serialPorts = new SerialPort[1];
    public string[] mSerialPorts = new string[1];
    private int mBandRate = 115200;
    private ArduinoMessageReceiver mReceiver;

    void Awake()
    {
        _serialPorts = new SerialPort[mSerialPorts.Length];
        mReceiver = FindObjectOfType<ArduinoMessageReceiver>();
        for (int i = 0; i < mSerialPorts.Length; ++i)
        {
            _serialPorts[i] = new SerialPort(mSerialPorts[i], mBandRate, Parity.None, 8, StopBits.One);
            Debug.Log("Connecting to " + mSerialPorts[i] + ".");
            PortConnect(i);
        }
    }

    //シリアル接続を開始する
    void PortConnect(int i)
    {
        if (_serialPorts[i] != null)
        {
            if (_serialPorts[i].IsOpen)
            {
                //既に開いているなら失敗
                _serialPorts[i].Close();
                Debug.LogError("Failed to open Serial Port, already open.");
            }
            else
            {
                //タイムアウト時間を設定し、接続開始
                _serialPorts[i].ReadTimeout = 50;
                _serialPorts[i].Open();
                Debug.Log("Success to open Serial Port.");
            }
        }
    }

    //i番目のシリアルポートを返す
    public static SerialPort serialPort(int i)
    {
        if (i < _serialPorts.Length) return _serialPorts[i];
        else return null;
    }

    //終了時はFastUpdate関数(arduinoにデータを送っている)を止め、モータなどの後処理をする
    void OnApplicationQuit()
    {
        mReceiver.Quit();

        ValveController[] v = FindObjectsOfType<ValveController>();
        PumpController[] p = FindObjectsOfType<PumpController>();
        foreach (ValveController vc in v) vc.CloseValve();
        foreach (PumpController pc in p) pc.SetPower(0);

        //シリアルを開いているなら、終了時に閉じる
        for (int i = 0; i < _serialPorts.Length; ++i)
        {
            if (_serialPorts[i] != null && _serialPorts[i].IsOpen) _serialPorts[i].Close();
        }
    }
}

