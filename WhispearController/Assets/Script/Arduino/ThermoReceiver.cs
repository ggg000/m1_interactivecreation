﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// ArduinoからのThermoMeterの情報を受け取る
/// </summary>
public class ThermoReceiver : ArduinoMessageProcessor_Base
{
    private float mCurrentBaseThermo = 0f;
    private float mCurrentTargetThermo = 0f;
    private int mCount = 0;

    public float mLowPathFilter = 0.1f;
    public int mCalibrationCount = 100;

    protected override void ProcessMessage(string message)
    {
        ProcessPositionMessage(message);
    }

    //まず来ないであろうという値を決めておく
    private float mTooBigValue = 1200f;
    private float mTooSmallValue = 500f;
    private float[] mPreThermo1 = new float[10];
    private float[] mPreThermo2 = new float[10];

    private float mTmpThermo1 = 0f;
    private float mTmpThermo2 = 0f;

    private int mCurrentCount;
    private bool mIsThermoCheckReady = false;

    //メッセージから位置を取得
    private void ProcessPositionMessage(string message)
    {
        char[] delimiterChars = { '/' };
        string[] s = message.Split(delimiterChars);
        if (s.Length != 2 || s[0].Length != 6 || s[1].Length != 6) return; //Arduinoの仕様上、6文字で来る(ex 400.00)

        //各温度情報の文字列を実際の数値に変換
        float t1 = 0f;
        float t2 = 0f;

        try
        {
            t1 = float.Parse(s[0]);
            t2 = float.Parse(s[1]);
        }
        catch (FormatException)
        {
            return;
        }

        //過大、過小な異常値は無視
        if (mTooBigValue < t1 || mTooSmallValue > t1) t1 = mCurrentBaseThermo;
        if (mTooBigValue < t2 || mTooSmallValue > t2) t2 = mCurrentTargetThermo;

        //過去十回分の受信値の平均を取る
        mPreThermo1[mCurrentCount] = t1;
        mPreThermo2[mCurrentCount] = t2;

        if (!mIsThermoCheckReady) mIsThermoCheckReady = (mCurrentCount == 9);

        mCurrentCount = (mCurrentCount + 1) % 10;
        mTmpThermo1 += t1;
        mTmpThermo2 += t2;

        if (mIsThermoCheckReady)
        {
            float val1 = mTmpThermo1 / 10f;
            float val2 = mTmpThermo2 / 10f;
            SetThermo(val1, val2);

            mTmpThermo1 -= mPreThermo1[mCurrentCount]; //一番古い値を消去
            mTmpThermo2 -= mPreThermo2[mCurrentCount]; //一番古い値を消去
        }

    }


    private void SetThermo(float t1, float t2)
    {
        if (mCount == 0)
        {
            //初期値を中心値とする
            mCount += 1;
            mCurrentBaseThermo = t1;
            mCurrentTargetThermo = t2;
        }
        else
        {
            if (mCount <= mCalibrationCount) mCount += 1;

            //中心値からの変位にパスをかけ、現在の情報に反映させる
            //実際の処理は、データが安定するまでしばらく間をあけてから行う
            mCurrentBaseThermo += mLowPathFilter * (t1 - mCurrentBaseThermo);
            mCurrentTargetThermo += mLowPathFilter * (t2 - mCurrentTargetThermo);
        }
    }

    public bool IsReady()
    {
        return mCount == mCalibrationCount;
    }

    public float GetCurrentBaseThermo()
    {
        return mCurrentBaseThermo;
    }

    public float GetCurrentTargetThermo()
    {
        return mCurrentTargetThermo;
    }

    //以下キャリブレーション
    //ポテンションの最大値と最小値に合わせてからボタンを押すと
    //以降はその値に応じてマッピングが行われる
    public Rect mLabelPos;
    public GUISkin mSkin;
    public bool mDebugShow = true;

    void OnGUI()
    {
        if (mDebugShow)
        {
            if (mSkin) GUI.skin = mSkin;

            GUI.Label(mLabelPos, "BaseThermo= " + mCurrentBaseThermo + " : " + mCurrentTargetThermo);

        }
    }
}
