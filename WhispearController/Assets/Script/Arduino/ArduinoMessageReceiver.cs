﻿using UnityEngine;
using System;
using System.IO.Ports;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

/// <summary>
/// Arduinoからのメッセージを受け取り、適切な対象に渡す
/// </summary>
public class ArduinoMessageReceiver : UpdateInterface
{

    private static bool _continue = true;
    private Thread _readThread;
    private static SerialPort mSerialPort;
    public int mSerialPortNumber = 0;
    public string[] mMessageTokens = new string[1];
    public ArduinoMessageProcessor_Base[] mMessageTargets = new ArduinoMessageProcessor_Base[1];
    private char[] mTrimDic = { '\0', '\r', '\n', '\t' };


    void Start()
    {
        //接続済みのシリアルポートを受け取り、メッセージ受信を開始する
        mSerialPort = ArduinoConnection.serialPort(mSerialPortNumber);
        if (mSerialPort != null)
        {
            _thread = new Thread(Read_Thread);
            _thread.Start();
        }
    }

    //void Update()
    //{
    //    //スレッドがメッセージを受信していれば、本プロセスで処理
    //    if (_thread_IsReceived&&_continue)
    //    {
    //        //System.Threading.Thread.Sleep(1);
    //        _thread_IsReceived = false;
    //        if (_thread_Message.Length > 0) SendArduinoMessage(_thread_Message);
    //    }
    //}

    public override void FastUpdate(float spf)
    {
        //スレッドがメッセージを受信していれば、本プロセスで処理
        if (_thread_IsReceived && _continue)
        {
            _thread_IsReceived = false;
            if (_thread_Message.Length > 0) SendArduinoMessage(_thread_Message);
        }
    }

    public void Quit()
    {
        _continue = false;
        Close_Thread();
    }

    //コルーチンで受信を繰り返し、メッセージ処理を行う
    private IEnumerator Read()
    {
        string value = "";
        while (_continue)
        {
            if (mSerialPort.IsOpen)
            {
                try
                {
                    //1行受信し、処理を行う
                    value = mSerialPort.ReadLine();
                    if (value.Length > 0) SendArduinoMessage(value.Trim(mTrimDic));
                }
                catch (TimeoutException)
                {
                }
                yield return new WaitForSeconds(0.0005f);
            }
        }
        yield return null;
    }

    private string _thread_Message = "";
    private bool _thread_IsReceived = false;
    private Thread _thread;
    //シリアルポートのリードを繰り返し、受信したら本プロセスに知らせるスレッド
    private void Read_Thread()
    {
        while (_continue && mSerialPort != null)
        {
            if (mSerialPort.IsOpen)
            {
                try
                {
                    _thread_Message = mSerialPort.ReadLine();
                    if (_thread_Message.Length > 0) _thread_IsReceived = true;
                }
                catch (TimeoutException)
                {
                }
            }
        }
    }

    //スレッド終了関数
    private void Close_Thread()
    {
        if (_thread != null && _thread.IsAlive) _thread.Join();
    }

    //各メッセージの一文字目に対応するオブジェクトにメッセージを丸投げする
    private void SendArduinoMessage(string message)
    {
        if ((message[0] != '\0') && (message[message.Length - 1].Equals('E'))) //末尾には必ずEを付けて送ってくる
        {
            //メッセージの一文字目を見て処理を切り替える
            for (int i = 0; i < mMessageTokens.Length; ++i)
            {
                if (message[0] == mMessageTokens[i][0])
                {
                    mMessageTargets[i].SendMessage("ProcessMessage", message.Substring(1, message.Length - 2));
                    return;
                }
            }
        }
    }


}
