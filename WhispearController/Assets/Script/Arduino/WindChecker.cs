﻿using UnityEngine;
using System.Collections;

public class WindChecker : UpdateInterface {
    [SerializeField]
    private ThermoReceiver mThermoReceiver;
    private float mWindValue = 0f;

    public override void FastUpdate(float spf)
    {
        mWindValue = mThermoReceiver.GetCurrentTargetThermo() - mThermoReceiver.GetCurrentBaseThermo();
    }

    public float GetWindValue()
    {
        return mWindValue;
    }
}
