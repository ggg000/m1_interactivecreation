﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Arduinoからのメッセージを受け取るオブジェクトの基底クラス
/// </summary>
abstract public class ArduinoMessageProcessor_Base : MonoBehaviour
{
    abstract protected void ProcessMessage(string message);
}
