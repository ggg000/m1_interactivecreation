﻿using UnityEngine;
using System;
using System.Collections;
using System.IO.Ports;
using System.Threading;

/// <summary>
/// シリアル通信でポンプに指令を送る
/// </summary>
public class PumpController : MonoBehaviour
{
    public string mPumpMessage = "P";
    public int mPortNumber = 0;
    private int mPumpPower = 0;

    private static SerialPort mSerialPort;
    private int mRewriteCount = 0;
    public static bool mUsing = false;
    public static System.Object lockObj = new System.Object();

    void Start()
    {
        mSerialPort = ArduinoConnection.serialPort(mPortNumber);
    }

    public int GetPumpPower()
    {
        return mPumpPower;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            SetPower(10);
        }
        else if (Input.GetKeyDown(KeyCode.Y))
        {
            SetPower(0);
        }
    }

    //ポンプで出力をする
    //同じ値なら無視
    public void SetPower(int power)
    {
        power = LimitPower(power);

        if (mPumpPower != power)
        {
            SendMessagePump(power);
        }
    }

    //powerを0..255で制限
    private int LimitPower(int power)
    {
        int p = 0;

        if (power > 255) p = 255;
        else if (power < 0) p = 0;
        else p = power;

        return p;
    }

    //ポンプを動かす指令を送る
    private void SendMessagePump(int power)
    {
        lock (lockObj) //同時には書き込まない
        {
            if (mSerialPort != null && mSerialPort.IsOpen)
            {
            label:
                try
                {
                    string writeMessage = mPumpMessage;
                    writeMessage = writeMessage + Mathf.Abs(power);

                    mSerialPort.Write(writeMessage + "/");
                    mPumpPower = power;
                    mRewriteCount = 0;
                }
                catch (Exception e)
                {
                    if (mRewriteCount < 10)
                    {
                        //失敗したら10回まで繰り返す
                        mRewriteCount += 1;
                        goto label;
                    }
                    else Debug.Log(e);
                }
            }
            //else StartCoroutine(ReWrite()); //失敗時はしばらく待ってもう一度
        }
    }

    IEnumerator ReWrite(int power)
    {
        yield return new WaitForSeconds(0.1f);
        SendMessagePump(power);
    }


    public GUISkin mSkin;
    public Rect mLabelRect;
    public bool mDebugShow = true;

    //実際に出力する電圧を表示
    void OnGUI()
    {
        if (mDebugShow)
        {
            if (mSkin) GUI.skin = mSkin;
            GUI.Label(mLabelRect, "PumpOutput=" + mPumpPower.ToString());
        }
    }
}
